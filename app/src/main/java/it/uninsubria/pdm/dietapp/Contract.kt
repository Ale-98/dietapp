package it.uninsubria.pdm.dietapp

class Contract {

    companion object{
        const val DATABASE_VERSION = 4
        const val DATABASE_NAME = "DBRecipes.db"
    }

    abstract class Recipe{
        companion object{
            const val TABLE_NAME = "recipe"
            const val KEY_ID = "id"
            const val KEY_TITLE = "title"
            const val KEY_DESCR = "descr"
            const val KEY_INGREDIENTS = "ingredients"
            const val KEY_COOKTIME = "cook_time"
            val COLUMNS = arrayOf(TABLE_NAME, KEY_ID, KEY_TITLE, KEY_DESCR, KEY_INGREDIENTS, KEY_COOKTIME)
        }
    }

    abstract class Ingredient{
        companion object{
            const val TABLE_NAME = "ingredient"
            const val KEY_ID = "id"
            const val KEY_NAME = "name"
            const val KEY_CARBO = "carbo"
            const val KEY_PROTEINS = "proteins"
            const val KEY_FAT = "fat"
            val COLUMNS = arrayOf(TABLE_NAME, KEY_ID, KEY_NAME, KEY_CARBO, KEY_PROTEINS, KEY_FAT)
        }
    }

}