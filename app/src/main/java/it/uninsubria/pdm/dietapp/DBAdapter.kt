package it.uninsubria.pdm.dietapp

import android.content.Context
import android.database.Cursor

class DBAdapter(context: Context) {

    private val helper:DBHelper = DBHelper(context)

    fun addRecipe(recipe: Recipe): Long{
        val db = helper.writableDatabase
        val values = recipe.getAsContentValues()
        recipe.id = db.insert(Contract.Recipe.TABLE_NAME, null, values)
        db.close()
        return recipe.id
    }

    fun getAllRecipes(): ArrayList<Recipe>{
        val recipes = ArrayList<Recipe>()

        val db = helper.readableDatabase

        val resultSet: Cursor = db.rawQuery(PredefinedQueries.SELECT_QUERIES[0], null)
        if(resultSet.moveToFirst())
            do {
                val id = resultSet.getLong(0)
                val title = resultSet.getString(1)
                val descr = resultSet.getString(2)
                val ingredients = resultSet.getString(3)
                val cooktime = resultSet.getString(4)
                val recipe = Recipe(title, descr, Recipe.ingredientsToArray(ingredients), cooktime)
                recipe.id = id
                recipes.add(recipe)
            }while (resultSet.moveToNext())
        resultSet.close()
        db.close()
        return recipes
    }

    fun deleteRecipe(recipe: Recipe) {

    }

    fun getIngredientValues(ingrName: String): ArrayList<Double>{
        val values = ArrayList<Double>()

        val db = helper.readableDatabase

        val resultSet: Cursor = db.rawQuery("select carbo, proteins, fat from ingredient where name = ?", arrayOf(ingrName))
        if (resultSet.moveToFirst()){
            val carbo = resultSet.getDouble(0)
            val proteins = resultSet.getDouble(1)
            val fat = resultSet.getDouble(2)
            values.addAll(arrayOf(carbo, proteins, fat))
        }
        resultSet.close()
        db.close()
        return values
    }

    fun getIngredientsNames(): ArrayList<String> {
        val names = ArrayList<String>()

        val db = helper.readableDatabase

        val resultSet: Cursor = db.rawQuery("select name from ingredient", null)
        if(resultSet.moveToFirst()){
            do{
                names.add(resultSet.getString(0)+":"+"100g") // Default quantity for ingredients in 100g
            }while (resultSet.moveToNext())
        }
        resultSet.close()
        db.close()
        return names
    }

}