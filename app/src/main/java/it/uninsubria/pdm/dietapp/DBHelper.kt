package it.uninsubria.pdm.dietapp

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context:Context): SQLiteOpenHelper(context, Contract.DATABASE_NAME, null, Contract.DATABASE_VERSION) {

    // Creting table Recipe
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(PredefinedQueries.CREATE_QUERIES[0])
        db?.execSQL(PredefinedQueries.CREATE_QUERIES[1])
        // Inizializza db con alcuni ingredienti
        addIngredients(db)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(PredefinedQueries.DROP_QUERIES[0])
        db?.execSQL(PredefinedQueries.DROP_QUERIES[1])
        db?.execSQL(PredefinedQueries.CREATE_QUERIES[0])
        db?.execSQL(PredefinedQueries.CREATE_QUERIES[1])
        addIngredients(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    private fun addIngredients(db: SQLiteDatabase?){
        for(ingredient:String in PredefinedQueries.INSERT_QUERIES){
            db?.execSQL(ingredient)
        }
    }
}