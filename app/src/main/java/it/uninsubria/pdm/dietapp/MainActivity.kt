package it.uninsubria.pdm.dietapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.tool_bar.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), RecyclerAdapter.OnRecipeListener{

    private val NEW_RECIPE_REQUEST = 1

    private val recipes = ArrayList<Recipe>()
    private lateinit var linearManager:LinearLayoutManager
    private lateinit var adapter:RecyclerAdapter
    private lateinit var recView:RecyclerView
    private lateinit var db:DBAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup for the toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setLogo(R.mipmap.ic_launcher)
        supportActionBar?.setDisplayUseLogoEnabled(true)
        supportActionBar?.title = "DietApp"

        // SetUp Database
        db = DBAdapter(this)
        recipes.addAll(db.getAllRecipes())

        linearManager = LinearLayoutManager(this)
        adapter = RecyclerAdapter(recipes, this)
        recView  = recipes_list
        // Associa layout manager alla recyclerView
        recView.layoutManager = linearManager
        // ...e anche l'adapter
        recView.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_activity_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if(id == R.id.action_add_recipe){
            val i = Intent(applicationContext, NewRecipeActivity::class.java)
            startActivityForResult(i, NEW_RECIPE_REQUEST)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    // Handler innescato al termine della subActivity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to
        if (requestCode == NEW_RECIPE_REQUEST) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                // Estrae i dati contenuti nell'intent ritornato dalla subActivity al clic della back-Arrow
                val title = data?.getStringExtra("TITLE")
                val cooktime = data?.getStringExtra("COOK_TIME")
                val description = data?.getStringExtra("DESCRIPTION")
                val ingredients = data?.getStringExtra("INGREDIENTS")

                Log.d(MainActivity::class.java.name, "onActivityResult() -> $title")
                if (title != null && cooktime != null && description != null && ingredients != null) {
                    // Chiama funzione per aggiungere nuova recipe al DB e alla listView tramite l'arrayList e l'adapter
                    addNewItem(Recipe(title, description, Recipe.ingredientsToArray(ingredients), cooktime))
                }
                return
            }
            else{
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
                return
            }
        }
    }

    private fun addNewItem(recipe: Recipe){
        // Dealing with DB
        db.addRecipe(recipe)
        // Adding to list and notifying adapter
        recipes.add(0, recipe)
        adapter.notifyDataSetChanged()
    }

    // Azione al click su una ricetta dell'elenco--> viene aperta l'activity con i dettagli sulla ricetta
    override fun onRecipeClick(position: Int) {
        // Toast.makeText(this, "Clicked", Toast.LENGTH_LONG).show()

        val currentRecipe:Recipe = recipes.get(position)

        val i = Intent(applicationContext, RecipeDetailsActivity::class.java)
        i.putExtra("title", currentRecipe.title)
        i.putExtra("cooktime", currentRecipe.cookTime)
        i.putExtra("description", currentRecipe.descr)
        i.putExtra("ingredients", currentRecipe.ingredients)
        startActivity(i)
    }

}
