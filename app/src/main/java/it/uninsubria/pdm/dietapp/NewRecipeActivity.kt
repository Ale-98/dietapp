package it.uninsubria.pdm.dietapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.MultiAutoCompleteTextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_recipe_details.*
import kotlinx.android.synthetic.main.new_recipe_activity_layout.*
import kotlinx.android.synthetic.main.tool_bar.*

class NewRecipeActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_recipe_activity_layout)

        // Settaggi per includere la toolbar(messa in un XML a parte e inclusa nei layout usando tag 'include'
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = "Add new Recipe"

        // Retrieving names of all ingredients for suggestions
        val db = DBAdapter(applicationContext)
        val ingredientNames = db.getIngredientsNames()
        val adapter = ArrayAdapter(applicationContext, android.R.layout.simple_list_item_1, ingredientNames)
        recipe_ingredients.setAdapter(adapter)
        recipe_ingredients.setTokenizer(MultiAutoCompleteTextView.CommaTokenizer())

        // Focus sul primo campo di testo
        recipe_title.requestFocus()
    }

    // Per annullare aggiunta ricetta
    override fun onSupportNavigateUp(): Boolean {
        setResult(Activity.RESULT_CANCELED)
        return true
    }

    // On click per aggiunta nuova ricetta
    fun confirmNewRecipe(view: View):Boolean{
        if(!checkAllFieldsFilled()) return false
        // Ritornare alla main activity le info necessarie
        val resultIntent = Intent()
        resultIntent.putExtra("TITLE", recipe_title.text.toString())
        resultIntent.putExtra("COOK_TIME", recipe_cooktime.text.toString())
        resultIntent.putExtra("DESCRIPTION", recipe_description.text.toString())
        resultIntent.putExtra("INGREDIENTS", recipe_ingredients.text.toString()) // Rifare per ottenere elenco ingredienti

        setResult(Activity.RESULT_OK, resultIntent)
        finish()
        return true
    }

    // Per controllare che siano stati riempiti tutti i campi prima di confermare
    private fun checkAllFieldsFilled():Boolean{
        val title = recipe_title.text
        val cooktime = recipe_cooktime.text
        val description = recipe_description.text
        val ingredients = recipe_ingredients.text
        if (title.isEmpty()){
            recipe_title.error = resources.getString(R.string.title_unfilled)
            return false
        }
        if (cooktime.isEmpty()){
            recipe_cooktime.error = resources.getString(R.string.cooktime_unfilled)
            return false
        }
        if (description.isEmpty()){
            recipe_description.error = resources.getString(R.string.description_unfilled)
            return false
        }
        if (ingredients.isEmpty()){
            recipe_ingredients.error = resources.getString(R.string.ingredients_unfilled)
            return false
        }
        return true
    }

}