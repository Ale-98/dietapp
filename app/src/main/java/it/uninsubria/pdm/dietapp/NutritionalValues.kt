package it.uninsubria.pdm.dietapp

import android.content.Context

class NutritionalValues(val name: String, val proteins: Double, val carbo: Double, val fat: Double) {

    companion object{
        fun computeNutritionalValues(context: Context, name:String, quantity:String):NutritionalValues{
            // Prendere dal db l'ingrediente con quel nome ed estrarne i valori nutrizionali
            val db = DBAdapter(context)
            val values = db.getIngredientValues(name)
            return NutritionalValues(name, equivalence(values[0], quantity),
                equivalence(values[1], quantity),
                equivalence(values[2], quantity))
        }

        private fun equivalence(value: Double, quantity: String) = (value/100)*Integer.parseInt(quantity.replaceAfter('g',"")
            .replace("g", "").trim())
    }

    fun computeCalories():Double = (proteins*4)+(carbo*4)+(fat*9)

}