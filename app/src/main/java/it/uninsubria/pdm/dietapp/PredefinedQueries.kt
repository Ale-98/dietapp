package it.uninsubria.pdm.dietapp

class PredefinedQueries {

    companion object{
        val CREATE_QUERIES = arrayOf(
            "create table if not exists ${Contract.Recipe.TABLE_NAME}(" +
                    "${Contract.Recipe.KEY_ID} integer primary key autoincrement," +
                    "${Contract.Recipe.KEY_TITLE} text not null," +
                    "${Contract.Recipe.KEY_DESCR} text," +
                    "${Contract.Recipe.KEY_INGREDIENTS} text," +
                    "${Contract.Recipe.KEY_COOKTIME} text)",

            // Contiene valori per 100g
            "create table if not exists ${Contract.Ingredient.TABLE_NAME}(" +
                    "${Contract.Ingredient.KEY_ID} integer primary key autoincrement," +
                    "${Contract.Ingredient.KEY_NAME} text," +
                    "${Contract.Ingredient.KEY_CARBO} integer," +
                    "${Contract.Ingredient.KEY_PROTEINS} integer," +
                    "${Contract.Ingredient.KEY_FAT} integer)")

        val INSERT_QUERIES = arrayOf(
            "insert into ingredient(name, carbo, proteins, fat) values(\"Farina avena\", 12.6, 7.1, 72.3)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Farina orzo\", 10.6, 1.9, 79.3)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Farina frumento duro\", 12.9, 2.8, 63.2)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Farina frumento integrale\", 11.9, 1.9, 67.8)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Farina frumento tipo 0\", 11.5, 1.0, 76.2)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Farina frumento tipo 00\", 11.0, 0.7, 77.3)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Farina mais\", 8.7, 2.7, 80.8)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Farina riso\", 7.3, 0.5, 87.0)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Farina segale\", 11.7, 2.3, 65.0)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Farina soia\", 36.8, 23.5, 23.4)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Fiocchi d’avena\", 8.0, 7.5, 72.8)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Grano saraceno\", 12.4, 3.3, 62.5)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Mais\", 9.2, 3.8, 75.1)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Miglio\", 11.8, 3.9, 72.9)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Riso\", 6.7, 0.4, 80.4)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Semola\", 11.5, 0.5, 76.9)" ,
            "insert into ingredient(name, carbo, proteins, fat) values(\"Tapioca\", 0.6, 0.2, 94.9)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Ceci\", 6.7, 2.3, 13.9)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Fagioli\", 23.6, 2.0, 47.5)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Fave\", 5.2, 0.4, 4.5)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Lenticchie\", 5.0, 0.5, 15.4)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Lupini\", 16.4, 2.4, 7.1)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Piselli\", 5.5, 0.6, 6.5)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Soia\", 36.9, 19.1, 23.2)",
            "insert into ingredient(name, carbo, proteins, fat) values(\"Acqua\", 0.0, 0.0, 0.0)"
        )

        val SELECT_QUERIES = arrayOf(
            "select * from ${Contract.Recipe.TABLE_NAME}"
        )

        val DROP_QUERIES = arrayOf(
            "drop table if exists ${Contract.Recipe.TABLE_NAME}",
            "drop table if exists ${Contract.Ingredient.TABLE_NAME}"
        )

    }

}