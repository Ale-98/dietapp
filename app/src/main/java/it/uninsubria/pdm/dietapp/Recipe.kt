package it.uninsubria.pdm.dietapp

import android.content.ContentValues
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.ArrayList

class Recipe(var title: String, var descr: String, var ingredients: ArrayList<String>, var cookTime: String) {

    var id:Long = -1

    // Da usare nella rappresentazione grafica
    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("Title: $title\tCook-Time: $cookTime\n")
        for(ingredient in ingredients){
            sb.append("-$ingredient\n")
        }
        sb.append("Description: $descr\n")
        return sb.toString()
    }

    fun getAsContentValues():ContentValues{
        val values = ContentValues()
        //values.put(Contract.Recipe.KEY_ID, id)
        values.put(Contract.Recipe.KEY_TITLE, title)
        values.put(Contract.Recipe.KEY_DESCR, descr)
        values.put(Contract.Recipe.KEY_INGREDIENTS, ingredientsToString(ingredients))
        values.put(Contract.Recipe.KEY_COOKTIME, cookTime)
        return values
    }

    companion object{
        fun ingredientsToString(ingredients:ArrayList<String>):String{
            val sb = StringBuilder()
            for(ingredient in ingredients){
                sb.append("$ingredient\n")
            }
            return sb.toString()
        }

        fun ingredientsToArray(ingredients:String):ArrayList<String>{
            val ingrList = ArrayList<String>()
            val stk = StringTokenizer(ingredients, ",")
            while (stk.hasMoreTokens()){
                ingrList.add(stk.nextToken().trim())
            }
            return ingrList
        }
    }


}