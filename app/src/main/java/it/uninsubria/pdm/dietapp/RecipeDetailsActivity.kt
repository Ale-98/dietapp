package it.uninsubria.pdm.dietapp

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_recipe_details.*
import kotlinx.android.synthetic.main.tool_bar.*
import lecho.lib.hellocharts.listener.PieChartOnValueSelectListener
import lecho.lib.hellocharts.model.PieChartData
import lecho.lib.hellocharts.model.SliceValue
import lecho.lib.hellocharts.util.ChartUtils
import lecho.lib.hellocharts.view.PieChartView
import java.util.*
import kotlin.collections.ArrayList


class RecipeDetailsActivity : AppCompatActivity() {

    private lateinit var piechart:PieChartView
    private lateinit var chartdata:PieChartData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_details)

        // Settaggi per includere la toolbar(messa in un XML a parte e inclusa nei layout usando tag 'include'
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = "Recipe details"

        details_title.text = intent.extras?.get("title") as CharSequence?
        details_cooktime.text = intent.extras?.get("cooktime") as CharSequence?
        details_description.text = intent.extras?.get("description") as CharSequence?
        val ingredientsList = intent.extras?.get("ingredients") as ArrayList<String>
        details_ingredients.text = Recipe.ingredientsToString(ingredientsList)

        piechart = pie_chart
        piechart.onValueTouchListener = ValueTouchListener(applicationContext)

        generateData(ingredientsList)
    }

    private fun generateData(ingredients: ArrayList<String>) {
        var slices = ArrayList<SliceValue>()

        var stk:StringTokenizer

        var name:String = ""
        var quantity:String = ""
        var values:NutritionalValues

        var calories = 0.0
        var proteins = 0.0
        var carbo = 0.0
        var fat = 0.0

        for(ing:String in ingredients){
            stk = StringTokenizer(ing,":")
            if(stk.hasMoreTokens()){
                name = stk.nextToken().trim()
                quantity = stk.nextToken().trim()
            }

            values = NutritionalValues.computeNutritionalValues(applicationContext, name, quantity)

            carbo += values.carbo
            proteins += values.proteins
            fat += values.fat
            calories += values.computeCalories()
        }

        // slices.add(SliceValue(calories.toFloat(), ChartUtils.pickColor()))
        slices.add(SliceValue(carbo.toFloat(), ChartUtils.pickColor()))
        slices.add(SliceValue(proteins.toFloat(), ChartUtils.pickColor()))
        slices.add(SliceValue(fat.toFloat(), ChartUtils.pickColor()))

        chartdata = PieChartData(slices)
        piechart.pieChartData = chartdata
        details_calories.text = (calories.toString()+"cal")
    }

    private class ValueTouchListener(val context: Context) : PieChartOnValueSelectListener {
        override fun onValueSelected(arcIndex: Int, value: SliceValue) {
            var nutValue = ""
            when (arcIndex){
                0 -> nutValue = "Carbo"
                1 -> nutValue = "Proteins"
                2 -> nutValue = "Fat"
            }
            Toast.makeText(context, "$nutValue: $value", Toast.LENGTH_SHORT).show();
        }

        override fun onValueDeselected() {
            // TODO Auto-generated method stub
        }
    }

}
