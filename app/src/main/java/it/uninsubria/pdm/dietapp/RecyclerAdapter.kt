package it.uninsubria.pdm.dietapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter(private val recipes: ArrayList<Recipe>, private val onrecipelistener: OnRecipeListener) :
    RecyclerView.Adapter<RecyclerAdapter.RecipeHolder>() {

    // Contenitore per le view che formano il layout della ricetta nella lista
    class RecipeHolder(
        private val row: View,
        private val onrecipelistener: OnRecipeListener
    ) : RecyclerView.ViewHolder(row), View.OnClickListener{

        private var recipe:Recipe? = null
        //var image: ImageView? = row.findViewById(R.id.recipe_image)
        private var title: TextView? = row.findViewById(R.id.recipe_title)
        private var description: TextView? = row.findViewById(R.id.recipe_description)

        // Associa una ricetta al proprio RecipeHolder
        fun bindRecipe(recipe:Recipe){
            this.recipe = recipe
            title?.text = recipe.title
            description?.text = recipe.descr
        }

        override fun onClick(v: View?) {
            onrecipelistener.onRecipeClick(adapterPosition)
        }

        init {
            row.setOnClickListener(this)
        }
    }

    /* Chiamato se non ci sono ViewHolder disponibili, nel caso ne viene creato uno nuovo
    *  assegandogli il layout con inflate
    */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeHolder {
        val inflatedView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.list_item_layout, parent, false)
        return RecipeHolder(inflatedView, onrecipelistener)
    }

    // Prende il ViewHolder e la posizione che avrà questo elemento nella lista
    override fun onBindViewHolder(holder: RecipeHolder, position: Int) {
        val recipe = recipes[position]
        holder.bindRecipe(recipe)
    }

    override fun getItemCount() = recipes.size

    interface OnRecipeListener{
        fun onRecipeClick(position: Int)
    }
}